<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Establishment;
use Illuminate\Support\Str;
use App\Models\Form;
use Auth;

class FormsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:form-list|form-create|form-edit|form-delete', ['only' => ['index','show']]);
        $this->middleware('permission:form-create', ['only' => ['create','store']]);
        $this->middleware('permission:form-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:form-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Form::latest()->paginate(20);

        return view('form.index',compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'fields' => 'required',
        ]);

        $form = new Form;
        $form->name = $request->input('name');
        $form->slug = Str::of($request->input('name'))->slug('-');
        $form->fields = json_encode( $request->input('fields') );
        $form->save();

        return redirect()->route('forms.index')
            ->with('success','Form successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Form $form)
    {
        $id = $request->input('es');
        $establishment = Establishment::find($id);

        if (!$establishment) {
            return redirect('establishments');
        }

        $right = null;
        $left = null;
        $fields = null;

        if ( $form->fields ) 
        {
            $fields = json_decode($form->fields);

            $right = $fields->right;
            $left = $fields->left;

            if (count($left)) 
            {   
                $newLeft = [];
                foreach ( $left as $l ) 
                {
                    $l = json_decode($l);
                    $l->slug = Str::slug($l->name, '_');
                    $newLeft[] = $l;
                }

                $left = $newLeft;
            }   

            if (count($right)) 
            {

                $newRight = [];
                foreach ( $right as $r ) 
                {
                    $r = json_decode($r);
                    $r->slug = Str::slug($r->name, '_');
                    $newRight[] = $r;
                }

                $right = $newRight;
            }
        }

        if ( $form->view ) {

            return view($form->view,compact('form','establishment','left','right','fields'));

        } else {

            return view('form.input',compact('form','establishment','left','right','fields'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        $right = null;
        $left = null;
        $fields = null;

        if ( $form->fields ) 
        {
            $fields = json_decode($form->fields);

            $right = $fields->right;
            $left = $fields->left;

            if ( count($left) )
            {
                $newLeft = [];
                foreach ( $left as $l ) 
                {
                    $l = json_decode($l);
                    $l->slug = Str::slug($l->name, '_');
                    $newLeft[] = $l;
                }

                $left = $newLeft;
            }

            if ( count($right) )
            {
                $newRight = [];
                foreach ( $right as $r ) 
                {
                    $r = json_decode($r);
                    $r->slug = Str::slug($r->name, '_');
                    $newRight[] = $r;
                }

                $right = $newRight;
            }
        }

        return view('form.edit',compact('form','left','right','fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'fields' => 'required',
        ]);

        $form = Form::find($id);
        $form->name = $request->input('name');
        $form->slug = Str::of($request->input('name'))->slug('-');
        $form->fields = json_encode( $request->input('fields') );
        $form->save();

        return redirect()->route('forms.edit',$id)
            ->with('success','Form successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        $form->delete();
        return redirect('forms')->with('success','Form successfully removed.');
    }
}
