<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use App\Models\Certification;
use App\Models\Establishment;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Form;
use Mail;

class PublicController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $events = Event::paginate(10);
        $forms = Form::all();

        return view('public.homepage',compact('events','forms'));
    }

    public function sendmail(Request $request)
    {
        $return = [
            'error' => true,
            'message' => 'Please try again.',
        ];

        $data = array(
            'name'=> $request->input('name'),
            'email'=> $request->input('email'),
            'subject'=> $request->input('subject'),
            'content'=> $request->input('content')
        );

        Mail::send(['text'=>'mail'], $data, function($message) use ($data) {
            $message->to('guko.vigeta911@gmail.com', 'BFPJaniuay')->subject($data['subject']);
            $message->from('bfpj@janiuayalert.ga','BFPJaniuay');
        });

        $return['message'] =  "Message sent successfully...";
        $return['error'] = false;
        
        return $return;
    }

    public function cron()
    {
        // Check all certifications
        $certs = Certification::where('status', '=', 'pending')->whereDate('created_at', Carbon::now()->subDays(4))->get();

        if ($certs->count()) 
        {
            foreach ($certs as $cert) 
            {
                $establishment = Establishment::find($cert->establishment_id);
                $form = Form::find($cert->form_id);

                if ( $establishment && $form )
                {
                    $message = 'Hi '.$establishment->owner_name.', we did not recieve any payment for your permit '. $form->name.'. ';
                    $message .= 'Kindly submit a new permit for your establishment '.$establishment->name.' and process your payment.';

                    $this->sendSMS($establishment->phone, $message);
                }

                $cert->delete();
            }
        }
    }

    public function sendSMS($phone,$message)
    {
        $args = [
          'phone' => $phone,
          'message' => $message,
          'code' => 'bfpj'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sms.iloiloshop.com");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args) );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

        // return $response;
    }
}
