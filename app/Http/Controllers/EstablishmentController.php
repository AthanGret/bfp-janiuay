<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Establishment;
use App\Models\Event;
use App\Models\Form;
use Auth;
use DB;

class EstablishmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:establishment-list|establishment-create|establishment-edit|establishment-delete', ['only' => ['index','show']]);
        $this->middleware('permission:establishment-create', ['only' => ['create','store']]);
        $this->middleware('permission:establishment-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:establishment-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $keyword = $request->input('s');

        $where = array();

        $establishments = Establishment::orderBy('created_at', 'desc');

        if ( $keyword ) {

            $establishments = $establishments->where('name','like','%'.$keyword.'%')
                ->orWhere('owner_name','like','%'.$keyword.'%');
        }

        if ( $user->hasRole('Client') ) {
            
            $establishments = $establishments->where('user_id','=',$user->id);
        }

        $establishments = $establishments->paginate(10);
        $events = Event::latest()->paginate(5);

        return view('establishments.index',compact('establishments','keyword','events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('establishments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'owner_name' => 'required',
            'phone' => 'required'
        ]);

        $user = Auth::user();
        $data = $request->all();
        $data['user_id'] = $user->id;

        Establishment::create($data);
    
        return redirect()->route('establishments.index')
                        ->with('success','Certification created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Establishment $establishment)
    {
        $forms = Form::all();

        $status = array(
            'pending' => 'orange',
            'approved' => 'cyan',
            'expired' => 'plum',
        );

        $message = array(
            'pending' => 'Your permit is currently being reviewed.',
            'approved' => 'Your permit is approved!',
            'expired' => 'Your permit has expired.',
        );

        return view('establishments.show',compact('establishment','forms','status','message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Establishment $establishment)
    {

        $user = Auth::user();

        if (!$user->hasRole('Admin')) {
            
            $arr = [
                'id' => $user->id,
                'user_id' => $establishment->user_id
            ];

            // dd($arr);

            if ($user->id != $establishment->user_id) {

                return redirect('establishments');
            }
        }

        return view('establishments.edit',compact('establishment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Establishment $establishment)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'address' => 'required',
            'owner_name' => 'required',
            'phone' => 'required'
        ]);

        $user = Auth::user();

        if (!$user->hasRole('Admin')) {
            if ($user->id != $establishment->user_id) {
                return redirect()->route('establishments.index')
                    ->with('error','Please try again.');
            }
        }

        $establishment->update($request->all());

        return redirect()->route('establishments.index')
                        ->with('success','Establishment updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
