<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use App\Models\Event;
use Hash;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = Auth::user();

        if ($user->hasRole('Admin')) {
            
            return redirect('certifications');

        } else {

            return redirect('/settings');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        $user = Auth::user();
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        $events = Event::latest()->paginate(5);
        return view('users.settings',compact('user','roles','userRole','events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settings_update(Request $request)
    {
        $user = Auth::user();

        $input = $request->all();

        $validate = [
            'name' => 'required',
            'password' => 'same:confirm-password',
            'gender' => 'required'
        ];

        if ($user->email != $input['email']) {
            $validate['email'] = 'required|email|unique:users,email,'.$user->id;
        }

        $this->validate($request, $validate);

        if(!empty($input['password']))
        {
            $input['password'] = Hash::make($input['password']);
        
        }else{
        
            $input = Arr::except($input,array('password'));    
        }
        
        $user->update($input);
    
        return redirect()->route('settings')
                        ->with('success','Details updated successfully!');
    }
}
