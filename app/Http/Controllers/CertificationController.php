<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Certification;
use App\Models\Establishment;
use Illuminate\Support\Str;
use App\Models\Form;
use Illuminate\Support\Carbon;
use Auth;

class CertificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:certification-list|certification-create|certification-edit|certification-delete', ['only' => ['index','show']]);
        $this->middleware('permission:certification-create', ['only' => ['create','store']]);
        $this->middleware('permission:certification-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:certification-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = array(
            'pending' => 'warning',
            'approved' => 'success',
            'expired' => 'danger'
        );

        $user = Auth::user();

        $keyword = $request->input('s');

        $where = array();

        $certifications = Certification::orderBy('created_at', 'desc');

        if ( $keyword ) {

            $certifications = $certifications->where('name','like','%'.$keyword.'%')
                ->orWhereHas('user', function($q) use ($keyword) {
                    return $q->where('name', 'LIKE', '%' . $keyword . '%');
                });
        }

        if ( $user->hasRole('Customer') ) {
            
            $certifications = $certifications->where('user_id','=',$user->id);
        }

        $certifications = $certifications->paginate(10);

        return view('certifications.index',compact('certifications','status','keyword'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $form = $request->input('form');
        // $id = $request->input('id');
        // $view = '';

        // $form = Form::find($form);
        // if ($form) { $view = $form->view; }

        // if ( empty($view) ) {
        //     return redirect('establishments/'.$id);
        // }

        // return view($view);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = [
            'name' => 'required',
            'form_id' => 'integer|required',
            'establishment_id' => 'integer|required',
            'contents' => 'required'
        ];

        $data = $request->all();
        $user = Auth::user();

        if ( $user->hasRole('Admin') || $user->hasRole('Employee') ) 
        {
            $valid_from = Carbon::parse($data['valid_from'])->format('Y-m-d');
            $valid_until = Carbon::parse($data['valid_until'])->format('Y-m-d');

            $validate['valid_from'] = 'required|date';
            $validate['valid_until'] = 'required|date|after:valid_from';

            $data['valid_from'] = $valid_from;
            $data['valid_until'] = $valid_until;

            if ( $request->input('status') ) {
                $data['status'] = $request->input('status');
            }
        }

        request()->validate($validate);
        
        $data['contents'] = json_encode($request->input('contents'));
        $data['user_id'] = $user->id;

        Certification::create($data);

        $establishment = Establishment::find($request->input('establishment_id'));
        $form = Form::find($request->input('form_id'));

        if ( $establishment && $form )
        {
            $message = 'Hi '.$establishment->owner_name.', we recieved your permit request for '. $form->name . '. ';
            $message .= 'Kindly settle your payment in our office before '. date('F d, Y', strtotime('+4 days'));
            $this->sendSMS($establishment->phone, $message);
        }
        
        return redirect()->route('establishments.show', $request->input('establishment_id'))
                        ->with('success','Certification created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Certification  $certification
     * @return \Illuminate\Http\Response
     */
    public function show(Certification $certification, Request $request)
    {
        $data = json_decode($certification->contents);

        if ($request->input('print')) {
            
            return view('certifications.print',compact('certification','data'));

        } else {

            return view('forms.show',compact('certification','data'));
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Certification  $certification
     * @return \Illuminate\Http\Response
     */
    public function edit(Certification $certification)
    {
        $data = json_decode($certification->contents);
        $establishment = $certification->establishment;
        $form = $certification->form;

        if (!$establishment) {
            return redirect('establishments');
        }

        $right = null;
        $left = null;
        $fields = null;

        if ( $form->fields ) 
        {
            $fields = json_decode($form->fields);

            $right = $fields->right;
            $left = $fields->left;

            if (count($left)) 
            {   
                $newLeft = [];
                foreach ( $left as $l ) 
                {
                    $l = json_decode($l);
                    $l->slug = Str::slug($l->name, '_');
                    $newLeft[] = $l;
                }

                $left = $newLeft;
            }   

            if (count($right)) 
            {

                $newRight = [];
                foreach ( $right as $r ) 
                {
                    $r = json_decode($r);
                    $r->slug = Str::slug($r->name, '_');
                    $newRight[] = $r;
                }

                $right = $newRight;
            }
        }

        // dd($data);
        $arr = [];
        foreach ($data as $key => $val) {
            $arr[$key] = $val;
        }

        $data = $arr;

        return view('certifications.edit',compact('certification','data','form','establishment','left','right','fields'));
    
        // return view($view,compact('certification','data','establishment','form'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Certification  $certification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Certification $certification)
    {
        $validate = [
            'name' => 'required',
            'form_id' => 'integer|required',
            'establishment_id' => 'integer|required',
            'contents' => 'required'
        ];

        $data = $request->all();
        $user = Auth::user();

        if ($user->hasRole('Admin') || $user->hasRole('Employee')) 
        {
            $valid_from = Carbon::parse($data['valid_from'])->format('Y-m-d');
            $valid_until = Carbon::parse($data['valid_until'])->format('Y-m-d');

            $validate['valid_from'] = 'required|date';
            $validate['valid_until'] = 'required|date|after:valid_from';

            $data['valid_from'] = $valid_from;
            $data['valid_until'] = $valid_until;

            if ( $request->input('status') ) {
                $data['status'] = $request->input('status');
            }
        }

        request()->validate($validate);
        
        $data['contents'] = json_encode($request->input('contents'));

        $certification->update($data);

        $establishment = Establishment::find($request->input('establishment_id'));
        $form = Form::find($request->input('form_id'));

        if ( $establishment && $form )
        {
            $message = '';

            if ($request->input('status') == 'approved') 
            {
                $message = 'Hi '.$establishment->owner_name.', your permit request for '. $form->name;
                $message .= ' has been approved and will expire on '. Carbon::parse($data['valid_until'])->format('F d, Y');
            }

            if ($request->input('status') == 'expired') 
            {
                $message = 'Hi '.$establishment->owner_name.', your permit '. $form->name;
                $message .= ' has expired. Kindly request a new permit for '.$establishment->name;
            }

            if ( !empty($message) ) 
            {
                $this->sendSMS($establishment->phone, $message);   
            }
        }
    
        return redirect()->route('certifications.index')
                        ->with('success','Certification updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Certification  $certification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Certification $certification)
    {
        $certification->delete();
    
        return redirect()->route('certifications.index')
                        ->with('success','Certification deleted successfully');
    }

    public function sendSMS($phone,$message)
    {
        $args = [
          'phone' => $phone,
          'message' => $message,
          'code' => 'bfpj'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sms.iloiloshop.com");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args) );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

        // return $response;
    }
}
