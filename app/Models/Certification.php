<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *	
     * @var array
     */
    protected $fillable = [
        'name', // name fo the form
        'contents', 
        'valid_from',
        'valid_until',
        'status',
        'user_id',
        'form_id',
        'establishment_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function form()
    {
        return $this->hasOne(Form::class,'id','form_id');
    }

    public function establishment()
    {
        return $this->hasOne(Establishment::class,'id','establishment_id');
    }
}