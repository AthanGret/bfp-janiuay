<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'address',
        'owner_name',
        'phone',
        'email',
        'user_id'
    ];

    public function certification()
    {
        return $this->hasOne(Certification::class,'establishment_id','id');
    }

    public function cert($id)
    {
        return Certification::where([
            ['form_id','=',$id],
            ['establishment_id','=',$this->id]
        ])->latest('created_at')->first();
    }
}
