@extends('neon')

@section('title')
<h2>Register New Certification</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            
            <a class="btn btn-default" href="javascript:;" onclick="window.history.back();"> Back</a>
            
        </div>
    </div>

    <hr>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('certifications.store') }}" method="POST">
    	@csrf
         <div class="row">
		    <div class="col-xs-12 col-sm-6 col-md-6">
		        <div class="form-group">
		            <strong>Name of Establishment:</strong>
		            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="">
		        </div>
                <div class="form-group">
                    <strong>Name of Owner/Representative:</strong>
                    <input type="text" name="owner_name" value="{{ old('owner_name') }}" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <strong>Purpose:</strong>
                    <select name="purpose" class="form-control">
                        <option value="For Business Permit (NEW/RENEWAL)">For business permit (NEW/RENEWAL)</option>
                        <option value="Certificate of Annual Inspection (PEZA)">Certificate of Annual Inspection (PEZA)</option>
                    </select>
                </div>
                <div class="form-group">
                    <strong>Address:</strong>
                    <input type="text" name="address" value="{{ old('address') }}" class="form-control" placeholder="Janiuay Public Market, Janiuay, Iloilo City">
                </div>
                <div class="form-group">
                    <strong>FSIC Number:</strong>
                    <input type="text" name="fsic_number" value="{{ old('fsic_number') }}" class="form-control" placeholder="FSIC Number">
                </div>
		    </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                @can('certification-delete')
                <div class="form-group">
                    <strong>Status:</strong>
                    <select name="status" class="form-control">
                        <option value="pending">Pending</option>
                        <option value="approved">Approved</option>
                        <option value="expired">Expired</option>
                    </select>
                </div>
                @endcan
                <div class="form-group">
                    <strong>Amount Paid:</strong>
                    <input type="text" name="amount_paid" value="{{ old('amount_paid') }}" class="form-control" placeholder="0.00">
                </div>
                <div class="form-group">
                    <strong>Reciept Number:</strong>
                    <input type="text" name="reciept_number" value="{{ old('reciept_number') }}" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <strong>Payment Date:</strong>
                    <input type="text" name="date_paid" value="{{ old('date_paid') }}" class="form-control" placeholder="MM/DD/YYYY">
                </div>
                @can('certification-delete')
                <div class="form-group">
                    <strong>Valid Until:</strong>
                    <input type="text" name="valid_until" value="{{ old('valid_until') }}" class="form-control" placeholder="MM/DD/YYYY">
                </div>
                @endcan
            </div>
            
		    <div class="col-xs-12 col-sm-12 col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>

@endsection