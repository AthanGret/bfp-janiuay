@extends('neon')

@section('title')
<h2>{{ $establishment->name }}</h2>
@endsection

@section('action')
    <a class="btn btn-primary icon-left btn-icon" href="{{ route('establishments.show', $establishment->id) }}">
        Go Back <i class="entypo-left-open"></i>
    </a>
@endsection

@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('certifications.update',$certification->id) }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="name" value="{{ $form->name }}">
        <input type="hidden" name="form_id" value="{{ $form->id }}">
        <input type="hidden" name="establishment_id" value="{{ $establishment->id }}">

        <div class="row">
            <div class="col-md-3">
                 <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Establishment Details</div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <table class="table">
                            <tr><td><b>Name:</b></td> <td>{{ $establishment->name }}</td></tr>
                            <tr><td><b>Nature of Business:</b></td> <td>{{ $establishment->description }}</td></tr>
                            <tr><td><b>Address:</b></td> <td>{{ $establishment->address }}</td></tr>
                            <tr><td><b>Owner:</b></td> <td>{{ $establishment->owner_name }}</td></tr>
                            <tr><td><b>Phone:</b></td> <td>{{ $establishment->phone }}</td></tr>
                            <tr><td><b>Email:</b></td> <td>{{ $establishment->email }}</td></tr>    
                        </table>

                    </div>
                </div>

                @can('certification-delete')
                    <div class="panel panel-default panel-shadow" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">Validity</div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <strong>Valid From:</strong>
                                <input type="date" name="valid_from" value="{{ old('valid_from') }}" class="form-control" placeholder="MM/DD/YYYY">
                            </div>
                            <div class="form-group">
                                <strong>Valid Until:</strong>
                                <input type="date" name="valid_until" value="{{ old('valid_until') }}" class="form-control" placeholder="MM/DD/YYYY">
                            </div>
                            <div class="form-group">
                                <strong>Status:</strong>
                                <select name="status" class="form-control">
                                    <option value="pending">Pending</option>
                                    <option value="approved">Approved</option>
                                    <option value="expired">Expired</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success btn-icon">Update <i class="entypo-check"></i></button>
                        </div>
                    </div>
                @endcan
                
            </div>
            <div class="col-md-9">
                
                <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">{{ $form->name }} Form</div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <h3 style="margin:0;">{{ $form->name }}</h3>
                        <hr>
                         <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">

                                @if( count($left) > 0 )
                                    @foreach( $left as $l => $m )
                                        @switch( $m->type )
                                        
                                            @case('text')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <input type="text" name="contents[{{ $m->slug }}]" value="{{ $data[$m->slug] }}" class="form-control">
                                                </div>

                                            @break
                                            @case('date')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <input type="date" placeholder="mm/dd/yyyy" name="contents[{{ $m->slug }}]" value="{{ $data[$m->slug] }}" class="form-control">
                                                </div>

                                            @break
                                            @case('textarea')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <textarea type="text" rows="5" name="contents[{{ $m->slug }}]" class="form-control">{{ $data[$m->slug] }}</textarea>
                                                </div>

                                            @break
                                            @case('select')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <select type="text" name="contents[{{ $m->slug }}]" class="form-control">

                                                        @if( $m->label )

                                                            @for( $i = 0; $i < count($m->label); $i++ )
                                                                <option value="{{ $m->value[$i] }}"{{ $data[$m->slug] == $m->value[$i] ? ' selected':'' }}>{{ $m->label[$i] }}</option>
                                                            @endfor

                                                        @endif
                                                
                                                    </select>
                                                </div>

                                            @break
                                            @case('checkbox')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>

                                                    @if( $m->label )
                                                    
                                                        @for( $i = 0; $i < count($m->label); $i++ ) 
                                                        
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="checkbox" name="contents[{{ $m->slug }}]" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}"{{ $data[$m->slug] == $m->value[$i] ? ' checked':'' }}>
                                                                <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                            </div>

                                                        @endfor

                                                    @endif

                                                </div>

                                            @break
                                            @case('radio')

                                                <h5>{{ $m->name }}</h5>
                                                @if( $m->label ) 
                                                    @for($i = 0; $i < count($m->label); $i++) 
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="contents[{{ $m->slug }}]" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}"{{ $data[$m->slug] == $m->value[$i] ? ' checked':'' }}>
                                                            <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                        </div>
                                                    @endfor
                                                @endif
                                    
                                            @break
                                        @endswitch
                                    @endforeach
                                @endif
                                
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                
                                @if( count($right) > 0 )
                                    @foreach( $right as $l => $m )
                                        @switch( $m->type )
                                        
                                            @case('text')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <input type="text" name="contents[{{ $m->slug }}]" value="{{ $data[$m->slug] }}" class="form-control">
                                                </div>

                                            @break
                                            @case('date')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <input type="date" placeholder="mm/dd/yyyy" name="contents[{{ $m->slug }}]" value="{{ $data[$m->slug] }}" class="form-control">
                                                </div>

                                            @break
                                            @case('textarea')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <textarea type="text" rows="5" name="contents[{{ $m->slug }}]" value="{{ $data[$m->slug] }}" class="form-control"></textarea>
                                                </div>

                                            @break
                                            @case('select')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>
                                                    <select type="text" name="contents[{{ $m->slug }}]" class="form-control">

                                                        @if( $m->label )

                                                            @for( $i = 0; $i < count($m->label); $i++ )
                                                                <option value="{{ $m->value[$i] }}"{{ $data[$m->slug] == $m->value[$i] ? ' selected':'' }}>{{ $m->label[$i] }}</option>
                                                            @endfor

                                                        @endif
                                                
                                                    </select>
                                                </div>

                                            @break
                                            @case('checkbox')

                                                <div class="form-group">
                                                    <label>{{ $m->name }}</label>

                                                    @if( $m->label )
                                                    
                                                        @for( $i = 0; $i < count($m->label); $i++ ) 
                                                        
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="checkbox" name="contents[{{ $m->slug }}]" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}"{{ $data[$m->slug] == $m->value[$i] ? ' checked':'' }}>
                                                                <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                            </div>

                                                        @endfor

                                                    @endif

                                                </div>

                                            @break
                                            @case('radio')

                                                <h5>{{ $m->name }}</h5>
                                                @if( $m->label ) 
                                                    @for($i = 0; $i < count($m->label); $i++) 
                                                        
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="contents[{{ $m->slug }}]" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}"{{ $data[$m->slug] == $m->value[$i] ? ' checked':'' }}>
                                                            <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                        </div>
                                                    
                                                    @endfor
                                                @endif

                                            @break
                                        @endswitch
                                    @endforeach
                                @endif
                                
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary btn-icon">Submit <i class="entypo-check"></i></button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection