<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Print</title>
        <style>
            table{border:1px solid #ccc;}
            table th{padding: 5px 10px;border:1px solid #ccc;background: #e8e8e8;text-align: left;}
            table td{padding: 5px 10px;border:1px solid #ccc;}
        </style>
    </head>
    <body>
        <div align="center">
            <h3>{{ $certification->name }}</h3>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $data as $key => $val )

                        @if( $key != '_token' )
                        <tr>
                            <td>{{ ucwords( str_replace('_', ' ', $key) ) }}</td>
                            <td>{{ $val }}</td>
                        </tr>
                        @endif

                    @endforeach
                    @if($certification->status!='pending')
                    <tr>
                        <td>Valid From</td>
                        <td>{{ \Illuminate\Support\Carbon::parse($certification->valid_from)->format('F j, Y') }}</td>
                    </tr>
                    <tr>
                        <td>Valid Until</td>
                        <td>{{ \Illuminate\Support\Carbon::parse($certification->valid_until)->format('F j, Y') }}</td>
                    </tr> 
                    @endif
                </tbody>
            </table>
        </div>
        <script>window.print();</script>
    </body>
</html>