@extends('neon')


@section('title')
<h2>Submitted Permit</h2>
@endsection

@section('action')
    
@endsection

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Submitted Forms</div>
        </div>
        
        <!-- panel body -->
        <div class="panel-body">
            
            <div class="row">
                <div class="col-md-3">

                    <form action="{{ route('certifications.index') }}" method="GET">
                        <div class="input-group">
                            <input type="text" name="s" value="{{ $keyword }}" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    <i class="entypo-search"></i>
                                </button>
                            </span>
                        </div>

                    </form>
                </div>
            </div>

            <hr>

            <div class="card">
                <div class="card-body">

                    @if( $certifications->count() )

                    <table class="table table-hover">
                        <tr>
                            <th>Status</th>
                            <th>Form</th>
                            <th>Customer</th>
                            <th>Valid From</th>
                            <th>Valid Until</th>
                            <th width="280px" class="text-right">Action</th>
                        </tr>
                        @foreach ($certifications as $certification)
                        <tr>
                            <td>
                                <span style="border-radius:50%;" class="btn btn-xs btn-{{ $status[$certification->status] }}">
                                    <b class="entypo-record"></b>
                                <span>
                            </td>
                            <td>
                                <a class="btn btn-link btn-xs" href="{{ route('certifications.show',$certification->id) }}">
                                    {{ $certification->name }}
                                </a>
                            </td>
                            <td>{{ $certification->user->name }}</td>
                            <td>
                                @if($certification->valid_from)
                                {{ \Illuminate\Support\Carbon::parse($certification->valid_from)->format('F j, Y') }}
                                @endif
                            </td>
                            <td>
                                @if($certification->valid_until)
                                {{ \Illuminate\Support\Carbon::parse($certification->valid_until)->format('F j, Y') }}
                                @endif
                            </td>
                            <td>
                                <div class="pull-right">
                                    @if($certification->status == 'expired')
                                        <a class="btn btn-info btn-xs btn-icon" href="{{ route('certifications.show',$certification->id) }}">View <i class="entypo-eye"></i></a>
                                    @endif
                                    @if($certification->status == 'approved')
                                        <a class="btn btn-xs btn-success btn-icon" target="_blank" href="{{ route('certifications.show',$certification->id) }}?print=1">Print<i class="entypo-print"></i></a>
                                    @endif
                                    @can('certification-edit')
                                        <a class="btn btn-primary btn-xs btn-icon{{ $certification->status == 'expired' ? ' disabled':'' }}" href="{{ route('certifications.edit',$certification->id) }}"{{ $certification->status == 'expired' ? ' disabled':'' }}>Edit <i class="entypo-pencil"></i></a>
                                    @endcan
                                    @can('certification-delete')
                                        <a href="javascript:;" onclick="jQuery('#modal-{{$certification->id}}').modal('show');" class="btn btn-danger btn-xs btn-icon">Delete <i class="entypo-cancel"></i></a>
                                    @endcan
                                </div>
                                <div class="modal fade" id="modal-{{$certification->id}}">
                                    <div class="modal-dialog">
                                        <form action="{{ route('certifications.destroy',$certification->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <div class="modal-content">
                                                
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Delete</h4>
                                                </div>
                                                
                                                <div class="modal-body">
                                                    Are you sure you wish to delete the form <b>{{ $certification->name }}</b> submitted by <b>{{ $certification->user->name }}</b>? 
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>

                    {!! $certifications->links() !!}

                    @else 

                    <div class="alert alert-info alert-dismissible show" role="alert">
                        @if($keyword)
                        <p>No results found.</p>
                        @else
                        <p>There are no available data to show at the moment.</p>
                        @endif
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>

                    @endif

                </div>
            </div>
        </div>
        
    </div>

   
    


    

@endsection