@extends('neon')

@section('title')
<h2>Manage Users</h2>
@endsection

@section('action')
    <a class="btn btn-success btn-icon" href="{{ route('users.create') }}"> Register New User <i class="entypo-plus"></i></a>
@endsection

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
    <div class="panel-heading">
        <div class="panel-title">User List</div>
    </div>
    <div class="panel-body">

        <form class="form-inline" method="GET" action="{{ route('users.index') }}">

            <label class="sr-only" for="role">Roles</label>
            <div class="input-group mb-2 mr-sm-2">
                <select class="form-control" name="role" id="role">
                    <option value="">All</option>
                    @if($roles)
                    @foreach($roles as $role)
                    <option value="{{ $role->name }}"{{ $role == $role->name ? ' selected':'' }}>{{ $role->name }}</option>
                    @endforeach
                    @endif
                </select>
            </div>

            <button type="submit" class="btn btn-primary btn-sm|btn-lg mb-2">Filter</button>
        </form>
        <br>
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($data as $key => $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                    <label class="badge badge-success">{{ $v }}</label>
                    @endforeach
                    @endif
                </td>
                <td>
                    <a class="btn btn-info btn-xs btn-icon" href="{{ route('users.show',$user->id) }}">Details <i class="entypo-eye"></i></a>
                    <a class="btn btn-primary btn-xs btn-icon" href="{{ route('users.edit',$user->id) }}">Edit <i class="entypo-pencil"></i></a>
                    @can('user-delete')
                    <a href="javascript:;" onclick="jQuery('#modal-{{$user->id}}').modal('show');" class="btn btn-danger btn-xs btn-icon">Delete <i class="entypo-cancel"></i></a>
                    <div class="modal fade" id="modal-{{$user->id}}">
                        <div class="modal-dialog">
                            <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="modal-content">
                                    
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete</h4>
                                    </div>
                                    
                                    <div class="modal-body">
                                        Are you sure you wish to delete this user?<br><br>
                                        <b>Name:</b> {{ $user->name }}<br>
                                        <b>Email:</b> {{ $user->email }}<br>
                                    </div>
                                    
                                    <div class="modal-footer">
                                        <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endcan
                </td>
            </tr>
            @endforeach
        </table>


        {!! $data->render() !!}



    </div>
    
</div>

@endsection