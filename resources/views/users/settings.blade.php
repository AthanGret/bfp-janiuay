@extends('neon')

@section('title')
<h2>Settings</h2>
@endsection

@section('content')

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

{!! Form::model($user, ['method' => 'PATCH','route' => ['settings_update', $user->id]]) !!}


<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-8">

        <div class="panel panel-default panel-shadow" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">Details</div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <strong>Name:</strong>
                    {!! Form::text('name', $user->name, array('placeholder' => 'Name','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Email:</strong>
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Gender:</strong>
                    {!! Form::select('gender', ['male'=>'Male','female'=>'Female'], [$user->gender], array('class' => 'form-control')) !!}
                </div>
            </div>
        </div>

        <div class="panel panel-default panel-shadow" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">Change Password</div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <strong>Password:</strong>
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <strong>Confirm Password:</strong>
                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary btn-icon">Save <i class="entypo-floppy"></i></button>

    </div>
    
    <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
            <div class="panel-heading">
                <div class="panel-title">Upcoming Events</div>
            </div>
            <div class="panel-body">

                @if( $events->count() )
                   
                    @foreach ($events as $event)

                        <div class="tile-stats tile-white-gray">
                            <div class="icon"><i class="entypo-calendar"></i></div>
                            <div class="num">{{ $event->name }}</div>
                            <h3>{{ $event->description }}</h3>
                            <p>{{ date('F d, Y', strtotime($event->start_date)) }}</p>
                        </div>
                            
                    @endforeach

                    {!! $events->links() !!}

                @else 
                    <div class="alert alert-info" role="alert">
                      There are no events to display at the moment.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@endsection