@extends('neon')

@section('title')
<h2>Create Event</h2>
@endsection

@section('action')
    <a href="{{ route('events.index') }}" class="btn btn-primary btn-icon icon-left">
        Go Back <i class="entypo-left-open"></i>
    </a>
@endsection

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="panel panel-default panel-shadow" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">Create Event</div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">
            <form action="{{ route('events.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <strong>Event Name:</strong>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="">
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Start Date:</strong>
                                    <input type="date" name="start_date" value="{{ old('start_date') }}" class="form-control" placeholder="MM/DD/YYYY">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>End Date:</strong>
                                    <input type="date" name="end_date" value="{{ old('end_date') }}" class="form-control" placeholder="MM/DD/YYYY">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            <textarea rows="5" name="description" class="form-control">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <button type="submit" class="btn btn-primary btn-icon">Submit <i class="entypo-check"></i></button>
                    </div>
                </div>
            </form>  
            
        </div>
    </div>

@endsection