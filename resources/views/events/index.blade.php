@extends('neon')


@section('title')
<h2>Events</h2>
@endsection

@section('action')
    <a href="{{ route('events.create') }}" class="btn btn-primary btn-icon icon-left">Create Event <i class="entypo-plus"></i></a>
@endsection

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Event List</div>
        </div>
        
        <!-- panel body -->
        <div class="panel-body">
            
            <div class="card">
                <div class="card-body">

                    @if( $events->count() )

                    <table class="table table-hover">
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>From</th>
                            <th>To</th>
                            <th width="280px" class="text-right">Action</th>
                        </tr>
                        @foreach ($events as $event)
                        <tr>
                            <td>{{ $event->name }}</td>
                            <td>{{ $event->description }}</td>
                            <td>{{ $event->start_date }}</td>
                            <td>{{ $event->end_date }}</td>
                            <td>
                                <div class="pull-right">
                                    {{-- <a class="btn btn-info btn-xs btn-icon" href="{{ route('events.show',$event->id) }}">View <i class="entypo-eye"></i></a> --}}
                                    @can('event-edit')
                                        <a class="btn btn-primary btn-xs btn-icon" href="{{ route('events.edit',$event->id) }}?form={{$event->slug}}">Edit <i class="entypo-pencil"></i></a>
                                    @endcan
                                    @can('event-delete')
                                        <a href="javascript:;" onclick="jQuery('#modal-{{$event->id}}').modal('show');" class="btn btn-danger btn-xs btn-icon">Delete <i class="entypo-cancel"></i></a>
                                    @endcan
                                </div>
                                <div class="modal fade" id="modal-{{$event->id}}">
                                    <div class="modal-dialog">
                                        <form action="{{ route('events.destroy',$event->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <div class="modal-content">
                                                
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Delete</h4>
                                                </div>
                                                
                                                <div class="modal-body">
                                                    Are you sure you wish to delete this event <b>{{ $event->name }}</b>? 
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>

                    {!! $events->links() !!}

                    @else 

                    <div class="alert alert-info alert-dismissible show" role="alert">
                        There are no available data to show at the moment.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    @endif

                </div>
            </div>
        </div>
        
    </div>

   
    


    

@endsection