@extends('neon')

@section('title')
<h2>{{ $establishment->name }}</h2>
@endsection

@section('action')
    <a class="btn btn-primary icon-left btn-icon" href="{{ route('establishments.show', $establishment->id) }}">
        Go Back <i class="entypo-left-open"></i>
    </a>
@endsection

@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('certifications.store') }}" method="POST">
        @csrf
        <input type="hidden" name="name" value="{{ $form->name }}">
        <input type="hidden" name="form_id" value="{{ $form->id }}">
        <input type="hidden" name="establishment_id" value="{{ $establishment->id }}">
        
        <div class="row">
            <div class="col-md-3">
                 <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Establishment Details</div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <table class="table">
                            <tr><td><b>Name:</b></td> <td>{{ $establishment->name }}</td></tr>
                            <tr><td><b>Nature of Business:</b></td> <td>{{ $establishment->description }}</td></tr>
                            <tr><td><b>Address:</b></td> <td>{{ $establishment->address }}</td></tr>
                            <tr><td><b>Owner:</b></td> <td>{{ $establishment->owner_name }}</td></tr>
                            <tr><td><b>Phone:</b></td> <td>{{ $establishment->phone }}</td></tr>
                            <tr><td><b>Email:</b></td> <td>{{ $establishment->email }}</td></tr>    
                        </table>
                    </div>
                </div>

                @can('certification-delete')
                    <div class="panel panel-default panel-shadow" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">Validity</div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <strong>Valid From:</strong>
                                <input type="date" name="valid_from" value="{{ old('valid_from') }}" class="form-control" placeholder="MM/DD/YYYY">
                            </div>
                            <div class="form-group">
                                <strong>Valid Until:</strong>
                                <input type="date" name="valid_until" value="{{ old('valid_until') }}" class="form-control" placeholder="MM/DD/YYYY">
                            </div>
                            <div class="form-group">
                                <strong>Status:</strong>
                                <select name="status" class="form-control">
                                    <option value="pending">Pending</option>
                                    <option value="approved">Approved</option>
                                    <option value="expired">Expired</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success btn-icon">Update <i class="entypo-check"></i></button>
                        </div>
                    </div>
                @endcan
                
            </div>
            <div class="col-md-9">
                
                <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Single and Two-family Dewlling Checklist</div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        
                    <h3>Single and Two-family Dewlling Checklist</h3>
                    <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Subject: <b>Conduct Inspection of</b></label>
                                    <input type="text" name="contents[subject]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Located at:</label>
                                    <input type="text" name="contents[located_at]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <input type="text" name="contents[date]" placeholder="MM/DD/YYYY" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Reference: <b>Inspection order number:</b></label>
                                    <input type="text" name="contents[inspection_order_number]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date Issued:</label>
                                    <input type="text" name="contents[date_issued]" placeholder="MM/DD/YYYY" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date of Inspection:</label>
                                    <input type="text" name="contents[date_of_inspection]" placeholder="MM/DD/YYYY" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4>Nature of Inspection Conducted:</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins1" value="Building Under Construction">
                                    <label class="form-check-label" for="ins1">Building Under Construction</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins2" value="Application For Occupancy Permit">
                                    <label class="form-check-label" for="ins2">Application For Occupancy Permit</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins3" value="Application For Business Permit">
                                    <label class="form-check-label" for="ins3">Application For Business Permit</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins4" value="Periodic Inspection Of Occupancy">
                                    <label class="form-check-label" for="ins4">Periodic Inspection Of Occupancy</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins5" value="Verification of Inspection Of Appliance to NTCV">
                                    <label class="form-check-label" for="ins5">Verification of Inspection Of Appliance to NTCV</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins6" value="Verification of Inspection of Complaint Received">
                                    <label class="form-check-label" for="ins6">Verification of Inspection of Complaint Received</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="contents[nature_of_inspection_conducted]" id="ins6" value="">
                                    <label class="form-check-label" for="ins6">Others (Specify):</label>
                                </div>
                                <input type="text" name="contents[other_nature_of_inspection]" class="form-control">
                            </div>
                        </div>
                        <hr>
                        <h4>General Information:</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Owner/Representative:</label>
                                    <input type="text" name="contents[owner_or_representative]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Total Land Area:</label>
                                    <input type="text" name="contents[total_land_area]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Address:</label>
                                    <input type="text" name="contents[address]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Total Floor Area:</label>
                                    <input type="text" name="contents[total_floor_area]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Contact Number:</label>
                                    <input type="text" name="contents[contact_number]" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Building Permit:</label>
                                    <input type="text" name="contents[building_permit]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date Issued:</label>
                                    <input type="text" name="contents[building_permit_date_issued]" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fire Code Fee:</label>
                                    <input type="text" name="contents[fire_code_fee]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>OR Number:</label>
                                    <input type="text" name="contents[or_number]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date Issued:</label>
                                    <input type="text" name="contents[or_number_date_issued]" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4>Building Construction:</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Beams:</label>
                                    <input type="text" name="contents[beams]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Exterior Walls:</label>
                                    <input type="text" name="contents[exterior_walls]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Main Stair:</label>
                                    <input type="text" name="contents[main_stair]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Main Door:</label>
                                    <input type="text" name="contents[main_door]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Columns:</label>
                                    <input type="text" name="contents[columns]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Corridor Walls:</label>
                                    <input type="text" name="contents[corridor_walls]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Windows:</label>
                                    <input type="text" name="contents[windows]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Trusses:</label>
                                    <input type="text" name="contents[trusses]" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Flooring:</label>
                                    <input type="text" name="contents[flooring]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Room Partisions:</label>
                                    <input type="text" name="contents[room_partitions]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Ceiling:</label>
                                    <input type="text" name="contents[ceiling]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Roof:</label>
                                    <input type="text" name="contents[roof]" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4>Sectional Occupancy:</h4>
                        <small>(Note: Indicate specific usage of each floor, section or rooms)</small>
                        <textarea rows="5" name="contents[sectional_occupancy]" class="form-control" required></textarea>
                        <hr>
                        <h4>Means of Egress:</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Readily Accessible?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[readily_accessible]" id="ra1" value="Yes" required>
                                                    <label class="form-check-label" for="ra1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[readily_accessible]" id="ra2" value="No" required>
                                                    <label class="form-check-label" for="ra2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Travel Distance Within?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[travel_distance_within]" id="td1" value="Yes" required>
                                                    <label class="form-check-label" for="td1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[travel_distance_within]" id="td2" value="No" required>
                                                    <label class="form-check-label" for="td2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Adequate Illumination?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[adequate_illumination]" id="ai1" value="Yes" required>
                                                    <label class="form-check-label" for="ai1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[adequate_illumination]" id="ai2" value="No" required>
                                                    <label class="form-check-label" for="ai2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Panic Hardware?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[panic_hardware]" id="ph1" value="Yes" required>
                                                    <label class="form-check-label" for="ph1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[panic_hardware]" id="ph2" value="No" required>
                                                    <label class="form-check-label" for="ph2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Doors Open Easily?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[doors_open_easily]" id="doe1" value="Yes" required>
                                                    <label class="form-check-label" for="doe1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[doors_open_easily]" id="doe2" value="No" required>
                                                    <label class="form-check-label" for="doe2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bldg with Mezzanine?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[bldg_with_mezzanine]" id="bwm1" value="Yes" required>
                                                    <label class="form-check-label" for="bwm1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[bldg_with_mezzanine]" id="bwm2" value="No" required>
                                                    <label class="form-check-label" for="bwm2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Obstructed?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[obstructed]" id="ob1" value="Yes" required>
                                                    <label class="form-check-label" for="ob1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[obstructed]" id="ob2" value="No" required>
                                                    <label class="form-check-label" for="ob2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Dead-ends Within Limits?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[dead_end_within_limit]" id="ddwl1" value="Yes" required>
                                                    <label class="form-check-label" for="ddwl1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[dead_end_within_limit]" id="ddwl2" value="No" required>
                                                    <label class="form-check-label" for="ddwl2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Proper Rating Of Illumination?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[proper_rating_of_illumination]" id="proi1" value="Yes" required>
                                                    <label class="form-check-label" for="proi1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[proper_rating_of_illumination]" id="proi2" value="No" required>
                                                    <label class="form-check-label" for="proi2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Door Swing in the Direction of?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[door_swing_in_the_direction_of]" id="ds1" value="Yes" required>
                                                    <label class="form-check-label" for="ds1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[door_swing_in_the_direction_of]" id="dw2" value="No" required>
                                                    <label class="form-check-label" for="dw2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Self-closure Operational?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[self_closure_operational]" id="sco1" value="Yes" required>
                                                    <label class="form-check-label" for="sco1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[self_closure_operational]" id="sco2" value="No" required>
                                                    <label class="form-check-label" for="sco2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mezzanine With Proper Exits?</td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[mezzanine_with_proper_exits]" id="mwpe1" value="Yes" required>
                                                    <label class="form-check-label" for="mwpe1">Yes</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="contents[mezzanine_with_proper_exits]" id="mwpe2" value="No" required>
                                                    <label class="form-check-label" for="mwpe2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <hr>
                        <h4>Defects / Defeciencies Noted During Inspection:</h4>
                        <small>(Attach Pitures Sketch and Others)</small>
                        <textarea rows="5" name="contents[defects]" class="form-control" required></textarea>
                        <hr>
                        <h4>Recomendations:</h4>
                        <textarea rows="5" name="contents[recomendations]" class="form-control" required></textarea>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary btn-icon">Submit <i class="entypo-check"></i></button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection