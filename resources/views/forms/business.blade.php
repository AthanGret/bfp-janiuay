@extends('neon')

@section('title')
<h2>Business Occupancy Checklist</h2>
@endsection

@section('action')
    <a class="btn btn-default" href="{{ route('certifications.index') }}">Go Back</a>
@endsection

@section('content')
    
    <hr>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('certifications.store') }}" method="POST">
        @csrf
         
            {{-- 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div> --}}
    


    </form>

@endsection