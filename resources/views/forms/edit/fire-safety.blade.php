@extends('neon')

@section('title')
<h2>{{ $establishment->name }}</h2>
@endsection

@section('action')
    <a class="btn btn-primary icon-left btn-icon" href="javascript:;" onclick="window.history.back();">
        Go Back <i class="entypo-left-open"></i>
    </a>
@endsection

@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('certifications.update',$certification->id) }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="name" value="{{ $form->name }}">
        <input type="hidden" name="form_id" value="{{ $form->id }}">
        <input type="hidden" name="establishment_id" value="{{ $establishment->id }}">
        
        <div class="row">
            <div class="col-md-3">
                 <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Establishment Details</div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <table class="table">
                            <tr><td><b>Name:</b></td> <td>{{ $establishment->name }}</td></tr>
                            <tr><td><b>Nature of Business:</b></td> <td>{{ $establishment->description }}</td></tr>
                            <tr><td><b>Address:</b></td> <td>{{ $establishment->address }}</td></tr>
                            <tr><td><b>Owner:</b></td> <td>{{ $establishment->owner_name }}</td></tr>
                            <tr><td><b>Phone:</b></td> <td>{{ $establishment->phone }}</td></tr>
                            <tr><td><b>Email:</b></td> <td>{{ $establishment->email }}</td></tr>    
                        </table>

                    </div>
                </div>

                @can('certification-delete')
                    <div class="panel panel-default panel-shadow" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">Validity</div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <strong>Valid From:</strong>
                                <input type="date" name="valid_from" value="{{ $certification->valid_from }}" class="form-control" placeholder="MM/DD/YYYY">
                            </div>
                            <div class="form-group">
                                <strong>Valid Until:</strong>
                                <input type="date" name="valid_until" value="{{ $certification->valid_until }}" class="form-control" placeholder="MM/DD/YYYY">
                            </div>
                            <div class="form-group">
                                <strong>Status:</strong>
                                <select name="status" class="form-control">
                                    <option value="pending"{{ $certification->status == 'pending' ? ' selected':'' }}>Pending</option>
                                    <option value="approved"{{ $certification->status == 'approved' ? ' selected':'' }}>Approved</option>
                                    <option value="expired"{{ $certification->status == 'expired' ? ' selected':'' }}>Expired</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success btn-icon">Update <i class="entypo-check"></i></button>
                        </div>
                    </div>
                @endcan
                
            </div>
            <div class="col-md-9">
                
                <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Fire Safety Inspection Certificate Form</div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <h3 style="margin:0;">Fire Safety Inspection Certificate</h3>
                        <hr>
                         <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Name of Establishment:</strong>
                                    <input type="text" name="contents[name]" value="{{ $data->name }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <strong>Name of Owner/Representative:</strong>
                                    <input type="text" name="contents[owner_name]" value="{{ $data->owner_name }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <strong>Purpose:</strong>
                                    <select name="contents[purpose]" class="form-control">
                                        <option value="For Business Permit (NEW/RENEWAL)"{{ $data->purpose == 'For Business Permit (NEW/RENEWAL)' ? ' selected':'' }}>For business permit (NEW/RENEWAL)</option>
                                        <option value="Certificate of Annual Inspection (PEZA)"{{ $data->purpose == 'Certificate of Annual Inspection (PEZA)' ? ' selected':'' }}>Certificate of Annual Inspection (PEZA)</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <strong>Address:</strong>
                                    <input type="text" name="contents[address]" value="{{ $data->address }}" class="form-control" placeholder="Janiuay Public Market, Janiuay, Iloilo City">
                                </div>
                                <div class="form-group">
                                    <strong>FSIC Number:</strong>
                                    <input type="text" name="contents[fsic_number]" value="{{ $data->fsic_number }}" class="form-control" placeholder="FSIC Number">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                
                                <div class="form-group">
                                    <strong>Amount Paid:</strong>
                                    <input type="text" name="contents[amount_paid]" value="{{ $data->amount_paid }}" class="form-control" placeholder="0.00">
                                </div>
                                <div class="form-group">
                                    <strong>Reciept Number:</strong>
                                    <input type="text" name="contents[reciept_number]" value="{{ $data->reciept_number }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <strong>Payment Date:</strong>
                                    <input type="date" name="contents[date_paid]" value="{{ $data->date_paid }}" class="form-control" placeholder="MM/DD/YYYY">
                                </div>
                                
                            </div>
                            
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary btn-icon">Submit <i class="entypo-check"></i></button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection