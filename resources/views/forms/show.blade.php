@extends('neon')

@section('title')
<h2>{{ $certification->name }}</h2>
@endsection

@section('action')
    <a class="btn btn-primary icon-left btn-icon" href="{{ route('establishments.show',$certification->establishment_id) }}">
        Go Back <i class="entypo-left-open"></i>
    </a>
@endsection

@section('content')
    
    <table class="table table-bordered table-inverse table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Details</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $data as $key => $val )

                @if( $key != '_token' )
                <tr>
                    <td>{{ ucwords( str_replace('_', ' ', $key) ) }}</td>
                    <td>{{ $val }}</td>
                </tr>
                @endif

            @endforeach
            @if($certification->status!='pending')
            <tr>
                <td>Valid From</td>
                <td>{{ \Illuminate\Support\Carbon::parse($certification->valid_from)->format('F j, Y') }}</td>
            </tr>
            <tr>
                <td>Valid Until</td>
                <td>{{ \Illuminate\Support\Carbon::parse($certification->valid_until)->format('F j, Y') }}</td>
            </tr> 
            @endif
            <tr>
                <td>Status</td>
                <td>{{ $certification->status }}</td>
            </tr>            
        </tbody>
    </table>
    

@endsection