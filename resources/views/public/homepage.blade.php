
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>BFP Janiuay</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
    <link href='{{asset('css/calendar.min.css')}}' rel='stylesheet' />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/line-awesome/css/line-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center  header-transparent header-scrolled">
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="/">BFP Janiuay</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li><a href="#about">About</a></li>
                    <li><a href="#services">Services</a></li>
                    <li><a href="#team">Team</a></li>
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif
                        
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        @can('certification-delete')
                            <li><a class="nav-link" href="{{ route('certifications.index') }}">Dashboard</a></li>
                        @endcan
                        @can('establishment-create')
                            <li><a class="nav-link" href="{{ route('establishments.index') }}">Establishments</a></li>
                        @endcan
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>
                        </li>
                    @endguest

                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex flex-column justify-content-end align-items-center">
        <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

            <div class="carousel-item active">
                <div class="carousel-container">
                    <h2 class="animate__animated animate__fadeInDown">Bureau Of Fire Protection</h2>
                    <p class="animate__animated fanimate__adeInUp">We commit to prevent and suppress distructive fires, investigate it's causes, enforce fire code and other related laws and respond to man-made natural disasters and other emergencies.</p>
                    <a href="tel:0335317101" class="btn-lg btn-orange btn">(033) 531-71-01</a>
                </div>
            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>

        <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
            <defs>
                <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
                </defs>
                <g class="wave1">
                    <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
                    </g>
                    <g class="wave2">
                        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
                        </g>
                        <g class="wave3">
                            <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
                            </g>
                        </svg>

                    </section><!-- End Hero -->

                    <main id="main">

                        <!-- ======= About Section ======= -->
                        <section id="about" class="about">
                            <div class="container">

                                <div class="row">
                                    <div class="col">
                                        <div class="section-title" data-aos="zoom-out">
                                            <h2>About</h2>
                                            <p>Our Mission</p>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="section-title" data-aos="zoom-out">
                                            <h2>About</h2>
                                            <p>Our Vission</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row content" data-aos="fade-up">
                                    <div class="col-lg-6">
                                        <p>We commit to prevent and suppress distructive fires, investigate it's causes, enforce fire code and other related laws and respond to man-made natural disasters and other emergencies.</p>
                                    </div>
                                    <div class="col-lg-6 pt-4 pt-lg-0">
                                        <p>A modern fire service fully capable of ensuring a fire-safe nation by 2034.</p>
                                    </div>
                                </div>

                            </div>
                        </section><!-- End About Section -->

                        @if($forms->count())
                        <section id="services" class="services">
                            <div class="container">

                                <div class="section-title" data-aos="zoom-out">
                                    <h2>Services</h2>
                                    <p>What we offer</p>
                                </div>

                                <div class="row">

                                    
                                    @foreach($forms as $form)

                                    <div class="col-lg-4 col-md-6">
                                        <div class="icon-box" data-aos="zoom-in-left">
                                            <div class="icon"><i class="las la-file-alt" style="color: #ff689b;"></i></div>
                                            <h4 class="title"><a href="{{ route('establishments.index') }}">{{ $form->name }}</a></h4>
                                            <p class="description"><a href="{{ route('establishments.index') }}" class="btn btn-orange">Get Certification</a></p>
                                        </div>
                                    </div>

                                    @endforeach
                                    

                                </div>

                            </div>
                        </section>
                        @endif

                        <!-- ======= F.A.Q Section ======= -->
                        <section id="events" class="faq">
                            <div class="container">

                                <div class="section-title" data-aos="zoom-out">
                                    <h2>Activities</h2>
                                    <p>Calendar of Events</p>
                                </div>

                                <div id='calendar'></div>

                            </div>
                        </section><!-- End F.A.Q Section -->

                        <!-- ======= Team Section ======= -->
                        <section id="team" class="team">
                            <div class="container">

                                <div class="section-title" data-aos="zoom-out">
                                    <h2>Team</h2>
                                    <p>Our Hardworking Team</p>
                                </div>

                                <div class="row">

                                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                                        <div class="member" data-aos="fade-up">
                                            <div class="member-img">
                                                <img src="{{asset('img/crew1.jpg')}}" class="img-fluid" alt="">
                                                <div class="social">
                                                    <a href=""><i class="icofont-twitter"></i></a>
                                                    <a href=""><i class="icofont-facebook"></i></a>
                                                    <a href=""><i class="icofont-instagram"></i></a>
                                                    <a href=""><i class="icofont-linkedin"></i></a>
                                                </div>
                                            </div>
                                            <div class="member-info">
                                                <h4>Walter White</h4>
                                                <span>Fire Fighter</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                                        <div class="member" data-aos="fade-up" data-aos-delay="100">
                                            <div class="member-img">
                                                <img src="{{asset('img/crew2.jpg')}}" class="img-fluid" alt="">
                                                <div class="social">
                                                    <a href=""><i class="icofont-twitter"></i></a>
                                                    <a href=""><i class="icofont-facebook"></i></a>
                                                    <a href=""><i class="icofont-instagram"></i></a>
                                                    <a href=""><i class="icofont-linkedin"></i></a>
                                                </div>
                                            </div>
                                            <div class="member-info">
                                                <h4>Michael Jhonson</h4>
                                                <span>Fire Fighter</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                                        <div class="member" data-aos="fade-up" data-aos-delay="200">
                                            <div class="member-img">
                                                <img src="{{asset('img/crew3.jpg')}}" class="img-fluid" alt="">
                                                <div class="social">
                                                    <a href=""><i class="icofont-twitter"></i></a>
                                                    <a href=""><i class="icofont-facebook"></i></a>
                                                    <a href=""><i class="icofont-instagram"></i></a>
                                                    <a href=""><i class="icofont-linkedin"></i></a>
                                                </div>
                                            </div>
                                            <div class="member-info">
                                                <h4>William Anderson</h4>
                                                <span>Fire Fighter</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                                        <div class="member" data-aos="fade-up" data-aos-delay="300">
                                            <div class="member-img">
                                                <img src="{{asset('img/crew4.jpg')}}" class="img-fluid" alt="">
                                                <div class="social">
                                                    <a href=""><i class="icofont-twitter"></i></a>
                                                    <a href=""><i class="icofont-facebook"></i></a>
                                                    <a href=""><i class="icofont-instagram"></i></a>
                                                    <a href=""><i class="icofont-linkedin"></i></a>
                                                </div>
                                            </div>
                                            <div class="member-info">
                                                <h4>Brad Jepson</h4>
                                                <span>Fire Fighter</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </section><!-- End Team Section -->

                        <!-- ======= Contact Section ======= -->
                        <section id="contact" class="contact">
                            <div class="container">

                                <div class="section-title" data-aos="zoom-out">
                                    <h2>Contact</h2>
                                    <p>Contact Us</p>
                                </div>

                                <div class="row mt-5">

                                    <div class="col-lg-4" data-aos="fade-right">
                                        <div class="info">
                                            <div class="address">
                                                <i class="icofont-google-map"></i>
                                                <h4>Location:</h4>
                                                <p>Janiuay, Iloilo City Philippines</p>
                                            </div>

                                            <div class="email">
                                                <i class="icofont-envelope"></i>
                                                <h4>Email:</h4>
                                                <p>help@bfpjaniuay.com</p>
                                            </div>

                                            <div class="phone">
                                                <i class="icofont-phone"></i>
                                                <h4>Call:</h4>
                                                <p>+1 5589 55488 55s</p>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left">

                                        <form id="contact-form" action="{{ route('sendmail') }}" method="post" role="form">
                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-6 form-group">
                                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                                    <div class="validate"></div>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                                    <div class="validate"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                                <div class="validate"></div>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" name="content" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                                <div class="validate"></div>
                                            </div>
                                            <div class="text-center">
                                                <button class="btn btn-warning btn-lg" type="submit">Send Message</button>
                                            </div>
                                        </form>

                                    </div>

                                </div>

                            </div>
                        </section><!-- End Contact Section -->

                    </main><!-- End #main -->

                    <!-- ======= Footer ======= -->
                    <footer id="footer">
                        <div class="container">
                            <h3>BFP Janiuay</h3>
                            <p>Janiuay, Iloilo City, Philippines.</p>
                            <div class="social-links">
                                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            </div>
                            <div class="copyright">
                                &copy; Copyright <strong><span>Selecao</span></strong>. All Rights Reserved
                            </div>
                            <div class="credits">
                            </div>
                        </div>
                    </footer><!-- End Footer -->

                    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

                    <!-- Vendor JS Files -->
                    <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
                    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
                    <script src="{{asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
                    <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
                    <script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
                    <script src="{{asset('assets/vendor/venobox/venobox.min.js')}}"></script>
                    <script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
                    <script src="{{asset('assets/vendor/aos/aos.js')}}"></script>
                    <script src="{{asset('js/calendar.min.js')}}"></script>

                    <!-- Template Main JS File -->
                    <script src="{{asset('assets/js/main.js')}}"></script>
                    <script>
                        document.addEventListener('DOMContentLoaded', function() {
                            var calendarEl = document.getElementById('calendar');
                            var calendar = new FullCalendar.Calendar(calendarEl, {
                                initialDate: '{{ date('Y-m-d') }}',
                                editable: true,
                                selectable: true,
                                businessHours: true,
                            dayMaxEvents: true, // allow "more" link when too many events
                            events: [
                                @if($events->count())
                                @foreach($events as $event)
                                {
                                    title: '{{ $event->name }}',
                                    start: '{{ $event->start_date }}',
                                    end: '{{ $event->end_date }}'
                                },
                                @endforeach
                                @endif
                            ]
                            });

                            calendar.render();
                        });

                        jQuery(document).ready(function() {

                            jQuery('#contact-form').submit(function(event) {

                                event.preventDefault();

                                var data = {
                                    _token: jQuery(this).find('[name="_token"]').val(),
                                    name: jQuery(this).find('[name="name"]').val(),
                                    email: jQuery(this).find('[name="email"]').val(),
                                    subject: jQuery(this).find('[name="subject"]').val(),
                                    content: jQuery(this).find('[name="content"]').val(),
                                };

                                $('[type="submit"]').html('Sending...');
                                $('[type="submit"]').prop('disabled',true);

                                $.ajax({
                                    url: '{{ route('sendmail') }}',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: data,
                                })
                                .always(function(e) {
                                    
                                    alert(e.message);

                                    $('[type="submit"]').html('Send Message');
                                    $('[type="submit"]').prop('disabled',false);
                                });
                                
                            });
                        });
                    </script><!-- comment -->
                </body>

                </html>