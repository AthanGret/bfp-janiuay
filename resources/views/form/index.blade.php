@extends('neon')


@section('title')
<h2>Forms & Certifications</h2>
@endsection

@section('action')
    <a href="{{ route('forms.create') }}" class="btn btn-primary btn-icon">Create Form <i class="entypo-plus"></i></a>
@endsection

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Submitted Forms</div>
        </div>
        
        <!-- panel body -->
        <div class="panel-body">
            
            <div class="card">
                <div class="card-body">

                    @if( $forms->count() )

                    <table class="table table-hover">
                        <tr>
                            <th>Name</th>
                            <th>Slug</th>
                            <th width="280px" class="text-right">Action</th>
                        </tr>
                        @foreach ($forms as $form)
                        <tr>
                            <td>{{ $form->name }}</td>
                            <td>{{ $form->slug }}</td>
                            <td>
                                <div class="pull-right">
                                    {{-- <a class="btn btn-info btn-xs btn-icon" href="{{ route('forms.show',$form->id) }}">View <i class="entypo-eye"></i></a> --}}
                                    @can('form-edit')
                                        <a class="btn btn-primary btn-xs btn-icon" href="{{ route('forms.edit',$form->id) }}">Edit <i class="entypo-pencil"></i></a>
                                    @endcan
                                    @can('form-delete')
                                        <a href="javascript:;" onclick="jQuery('#modal-{{$form->id}}').modal('show');" class="btn btn-danger btn-xs btn-icon">Delete <i class="entypo-cancel"></i></a>
                                    @endcan
                                </div>
                                <div class="modal fade" id="modal-{{$form->id}}">
                                    <div class="modal-dialog">
                                        <form action="{{ route('forms.destroy',$form->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <div class="modal-content">
                                                
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Delete</h4>
                                                </div>
                                                
                                                <div class="modal-body">
                                                    Are you sure you wish to delete this form:<br> <b>{{ $form->name }}</b>? 
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>

                    {!! $forms->links() !!}

                    @else 

                    <div class="alert alert-info alert-dismissible show" role="alert">
                        <p>There are no available forms to show at the moment.</p><br>
                        <a href="{{ route('forms.create') }}" class="btn btn-primary btn-icon">Create Form <i class="entypo-plus"></i></a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    @endif

                </div>
            </div>
        </div>
        
    </div>

   
    


    

@endsection