@extends('neon')

@section('title')
<h2>Edit Form</h2>
@endsection

@section('action')
<a class="btn btn-default" href="javascript:;" onclick="window.history.back();">Go Back</a>
@endsection

@section('header')
<style>
.form-check-label{margin-left: 8px;margin-bottom: 0px;}
#left .panel, #right .panel{cursor: move;}
.sortable-placeholder{ 
    border: 1px solid dotted; 
    background-color: #f0f0f1; 
    height: 137px;
    margin-bottom: 17px;
    border-radius: 5px;
}
</style>
@endsection

@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('forms.update',$form->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-shadow" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">Details</div>
                        <div class="panel-options"></div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <strong>Form Name:</strong>
                            <input type="text" name="name" value="{{ $form->name }}" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="fields">

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="panel panel-default panel-shadow" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">Left Fields</div>
                            <div class="panel-options">
                                <a href="#left-modal" data-toggle="modal" data-target="#left-modal" class="bg">
                                    &nbsp;Add Field <i class="entypo-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div id="left" class="panel-body">

                            @if( count($left) > 0 )
                                @foreach( $left as $l => $m )
                                    
                                    <div class="panel panel-info" data-collapsed="0">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                @if(isset($m->editable) && $m->editable)
                                                    Client
                                                @else
                                                    Admin
                                                @endif
                                            </div>
                                            <div class="panel-options">
                                                <a href="#" onclick="deletePanel(this)"><i class="entypo-cancel"></i></a>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <input type="hidden" name="fields[left][]" value='{{ json_encode($m) }}'>
                                            
                                            @switch( $m->type )
                                            
                                                @case('text')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <input type="text" class="form-control">
                                                    </div>

                                                @break
                                                @case('date')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <input type="date" placeholder="mm/dd/yyyy" class="form-control">
                                                    </div>

                                                @break
                                                @case('textarea')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <textarea type="text" rows="5" class="form-control"></textarea>
                                                    </div>

                                                @break
                                                @case('select')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <select type="text" class="form-control">

                                                            @if( $m->label )

                                                                @for( $i = 0; $i < count($m->label); $i++ )
                                                                    <option value="{{ $m->value[$i] }}">{{ $m->label[$i] }}</option>
                                                                @endfor

                                                            @endif
                                                    
                                                        </select>
                                                    </div>

                                                @break
                                                @case('checkbox')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>

                                                        @if( $m->label )
                                                        
                                                            @for( $i = 0; $i < count($m->label); $i++ ) 
                                                            
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="checkbox" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}">
                                                                    <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                                </div>

                                                            @endfor

                                                        @endif

                                                    </div>

                                                @break
                                                @case('radio')

                                                    <table class="table table-bordered" style="margin-bottom:0px;">
                                                        <tbody>
                                                            <tr>
                                                                <td>{{ $m->name }}</td>
                                                                @if( $m->label ) 
                                                                    @for($i = 0; $i < count($m->label); $i++) 
                                                                        <td>
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}">
                                                                                <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                                            </div>
                                                                        </td>
                                                                    @endfor
                                                                @endif
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                @break

                                            @endswitch
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                   <div class="panel panel-default panel-shadow" data-collapsed="0">

                        <div class="panel-heading">
                            <div class="panel-title">Right Fields</div>
                            <div class="panel-options">
                                <a href="#right-modal" data-toggle="modal" data-target="#right-modal" class="bg">
                                    &nbsp;Add Field <i class="entypo-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div id="right" class="panel-body">
                            @if( count($right) > 0 )
                                @foreach( $right as $l => $m )
                                    
                                    <div class="panel panel-info" data-collapsed="0">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                @if(isset($m->editable) && $m->editable)
                                                    Client
                                                @else
                                                    Admin
                                                @endif
                                            </div>
                                            <div class="panel-options">
                                                <a href="#" onclick="deletePanel(this)"><i class="entypo-cancel"></i></a>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <input type="hidden" name="fields[right][]" value='{{ json_encode($m) }}'>
                                            
                                            @switch( $m->type )
                                            
                                                @case('text')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <input type="text" class="form-control">
                                                    </div>

                                                @break
                                                @case('date')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <input type="date" placeholder="mm/dd/yyyy" class="form-control">
                                                    </div>

                                                @break
                                                @case('textarea')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <textarea type="text" rows="5" class="form-control"></textarea>
                                                    </div>

                                                @break
                                                @case('select')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>
                                                        <select type="text" class="form-control">

                                                            @if( $m->label )

                                                                @for( $i = 0; $i < count($m->label); $i++ )
                                                                    <option value="{{ $m->value[$i] }}">{{ $m->label[$i] }}</option>
                                                                @endfor

                                                            @endif
                                                    
                                                        </select>
                                                    </div>

                                                @break
                                                @case('checkbox')

                                                    <div class="form-group">
                                                        <label>{{ $m->name }}</label>

                                                        @if( $m->label )
                                                        
                                                            @for( $i = 0; $i < count($m->label); $i++ ) 
                                                            
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="checkbox" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}">
                                                                    <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                                </div>

                                                            @endfor

                                                        @endif

                                                    </div>

                                                @break
                                                @case('radio')

                                                    <table class="table table-bordered" style="margin-bottom:0px;">
                                                        <tbody>
                                                            <tr>
                                                                <td>{{ $m->name }}</td>
                                                                @if( $m->label ) 
                                                                    @for($i = 0; $i < count($m->label); $i++) 
                                                                        <td>
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="{{ $m->slug.$i }}" value="{{ $m->value[$i] }}">
                                                                                <label class="form-check-label" for="{{ $m->slug.$i }}">{{ $m->label[$i] }}</label>
                                                                            </div>
                                                                        </td>
                                                                    @endfor
                                                                @endif
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                @break

                                            @endswitch
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" class="btn btn-primary btn-icon">Save <i class="entypo-check"></i></button>
                </div>
            </div>

        </div>

    </form>

    <div class="modal gray fade" id="left-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Insert Field</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <p>
                            <label>Label:</label>
                            <input name="name" data-target="left" type="text" class="form-control name">
                            <input name="slug" type="hidden" value="">
                        </p>
                        <p>
                            <label>Field Type:</label>
                            <select name="type" data-target="left" class="form-control" disabled>
                                <option value="text">Text Field</option>
                                <option value="date">Date Field</option>
                                <option value="textarea">Description Box</option>
                                <option value="checkbox">Checkbox</option>
                                <option value="select">Dropdown Menu</option>
                                <option value="radio">Radio Selection</option>
                            </select>
                        </p>
                        <br>
                        <p>
                            <div class="make-switch" 
                                data-text-label="<i class='entypo-user'></i>" 
                                data-on-label="Client" 
                                data-off-label="Admin">
                                <input type="checkbox" name="editable" value="1" data-target="left" checked />
                            </div>
                            <small>&nbsp;Who can fill up this field?</small> 
                        </p>
                        <div class="contents"></div>
                        <div class="action"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" data-target="left" onclick="insert(this)" class="btn btn-primary">Insert</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal gray fade" id="right-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Insert Field</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <p>
                            <label>Label:</label>
                            <input name="name" data-target="right" type="text" class="form-control name">
                            <input name="slug" type="hidden" value="">
                        </p>
                        <p>
                            <label>Field Type:</label>
                            <select name="type" data-target="right" class="form-control" disabled>
                                <option value="text">Text Field</option>
                                <option value="date">Date Field</option>
                                <option value="textarea">Description Box</option>
                                <option value="checkbox">Checkbox</option>
                                <option value="select">Dropdown Menu</option>
                                <option value="radio">Radio Selection</option>
                            </select>
                        </p>
                        <br>
                        <p>
                            <div class="make-switch" 
                                data-text-label="<i class='entypo-user'></i>" 
                                data-on-label="Client" 
                                data-off-label="Admin">
                                <input type="checkbox" name="editable" value="1" data-target="right" checked />
                            </div>
                            <small>&nbsp;Who can fill up this field?</small> 
                        </p>
                        <div class="contents"></div>
                        <div class="action"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" data-target="right" onclick="insert(this)" class="btn btn-primary">Insert</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer')
<script src="{{asset('assets/js/bootstrap-switch.min.js')}}"></script>
<script>
jQuery(document).ready(function($) {
    $('.modal .name').blur(function(){

        var val = $(this).val();
        var target = $(this).data('target');

        if ( val=='' ) {

            $('#'+target+'-modal select').prop('disabled',true);
            $('#'+target+'-modal .contents').html('');
            $('#'+target+'-modal .action').html('');

        } else {

            $('#'+target+'-modal select').prop('disabled',false);
        }
    });

    $('.modal select').change(function(event) {

        var target = $(this).data('target');
        var val = $(this).val();
        var slug = slugify( $('#'+target+'-modal .name').val() );

        $('#'+target+'-modal [name="slug"]').val(slug);
        $('#'+target+'-modal .contents').html('');
        $('#'+target+'-modal .action').html('');

        var haystack = ['checkbox','select','radio'];

        if ( inArray(val, haystack) ) {
            $('#'+target+'-modal .contents').html('<hr>');
            addInput('#'+target+'-modal .contents');
            $('#'+target+'-modal .action').html('<button type="button" onclick="addInput(\'#'+target+'-modal .contents\')" class="btn btn-default btn-sm">Add Field</button>');
        }
    });

    $('#left').sortable({
      placeholder: "sortable-placeholder"
    }).disableSelection();
    $('#right').sortable({
      placeholder: "sortable-placeholder"
    }).disableSelection();
});

function insert(btn)
{
    var target = jQuery(btn).data('target');
    var data = toJSON(jQuery('#'+target+'-modal form'));
    inputType(data, target);

    jQuery('#'+target+'-modal').modal('toggle');
}

function slugify(txt) {
    return txt.toLowerCase().replace(/ /g,'_').replace(/[^\w-]+/g,'');
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function toJSON(form) 
{
    var o = {};
    var a = form.serializeArray();

    for ( var key in a ) {
        if ( a.hasOwnProperty( key ) ) {
            if (o[a[key].name]) {
                if (!o[a[key].name].push) {
                    o[a[key].name] = [o[a[key].name]];
                }
                o[a[key].name].push(a[key].value || '');
            } else {
                o[a[key].name] = a[key].value || '';
            }
        }
    }

    return o;
}

function addInput(target){

    var input = '<div class="row">'+
        '<div class="col-md-6 col-sm-6 col-xs-6">'+
            '<p><label>Label:</label><input name="label" type="text" class="form-control"></p>'+
        '</div>'+
        '<div class="col-md-6 col-sm-6 col-xs-6">'+
            '<label>Value:</label>'+
            '<div class="input-group">'+
                '<input name="value" type="text" class="form-control">'+
                '<span class="input-group-btn">'+
                    '<button type="button" class="btn btn-danger" onclick="jQuery(this).parent().parent().parent().parent().remove();" type="button"><i class="entypo-cancel"></i></button>'+
                '</span>'+
            '</div>'+
        '</div>'+
    '</div>';

    jQuery(target).append(input);
}

function inputType(obj,target)
{   
    var html = '';
    var title = '';
    if ( obj.editable ) {
        title = 'Client';
    } else {
        title = 'Admin';
    }

    switch(obj.type)
    {
        case 'text':
            html += '<div class="form-group"><label>'+obj.name+'</label><input type="text" class="form-control"></div>';
        break;
        case 'date':
            html += '<div class="form-group"><label>'+obj.name+'</label><input type="date" placeholder="mm/dd/yyyy" class="form-control"></div>';
        break;
        case 'textarea':
            html += '<div class="form-group"><label>'+obj.name+'</label><textarea type="text" rows="5" class="form-control"></textarea></div>';
        break;
        case 'select':
            html += '<div class="form-group"><label>'+obj.name+'</label>';
            html += '<select type="text" class="form-control">';
            if (obj.label.length > 0) {
                for (var i = 0; i < obj.label.length; i++) {
                    html += '<option value="'+obj.value[i]+'">'+obj.label[i]+'</option>';
                }
            }
            html += '</select></div>';
        break;
        case 'checkbox':

            html += '<div class="form-group"><label>'+obj.name+'</label>';

            if (obj.label.length > 0) 
            {
                for (var i = 0; i < obj.label.length; i++) 
                {
                    html += '<div class="form-check form-check-inline">';
                    html += '<input class="form-check-input" type="checkbox" id="'+obj.slug+i+'" value="'+obj.value[i]+'">';
                    html += '<label class="form-check-label" for="'+obj.slug+i+'">'+obj.label[i]+'</label>';
                    html += '</div>';
                }
            }

        break;
        case 'radio':

            html += '<table class="table table-bordered" style="margin-bottom:0px;">';
            html += '<tbody><tr>';
            html += '<td>'+obj.name+'</td>';
           
            if (obj.label.length > 0) 
            {
                for (var i = 0; i < obj.label.length; i++) 
                {
                    html += '<td>';
                    html += '<div class="form-check form-check-inline">';
                    html += '<input class="form-check-input" type="radio" id="'+obj.slug+i+'" value="'+obj.value[i]+'">';
                    html += '<label class="form-check-label" for="'+obj.slug+i+'">'+obj.label[i]+'</label>';
                    html += '</div>';
                    html += '</td>';
                }
            }
            
            html += '</tr></tbody>';
            html += '</table>';

        break;
    }

    var newHtml = '<div class="panel panel-info" data-collapsed="0">';
        newHtml += '<div class="panel-heading">';
        newHtml += '<div class="panel-title">'+title+'</div>';
        newHtml += '<div class="panel-options">';
        newHtml += '<a href="#" onclick="deletePanel(this)"><i class="entypo-cancel"></i></a>';
        newHtml += '</div>';
        newHtml += '</div>';
        newHtml += '<div class="panel-body">';
        newHtml += '<input type="hidden" name="fields['+target+'][]" value=\''+JSON.stringify(obj)+'\'>';
        newHtml += html;
        newHtml += '</div>';
        newHtml += '</div>';

    jQuery('#'+target).append(newHtml);
}

function deletePanel(btn)
{
    jQuery(btn).parent().parent().parent().remove();
}
</script>
@endsection