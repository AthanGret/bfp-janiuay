@extends('neon')

@section('title')
<h2>Certification Details</h2>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
           
            <a class="btn btn-default" href="javascript:;" onclick="window.history.back();"> Back</a>
            
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name of Establishment:</strong>
                {{ $certification->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name of Owner/Representative:</strong>
                {{ $certification->owner_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Address:</strong>
                {{ $certification->address }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>FSIC Number:</strong>
                {{ $certification->fsic_number }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Purpose:</strong>
                {{ $certification->purpose }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Amount Paid:</strong>
                {{ $certification->amount_paid }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Payment Date:</strong>
                {{ $certification->date_paid }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Valid Until:</strong>
                {{ $certification->valid_until }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                {{ $certification->status }}
            </div>
        </div>
    </div>

@endsection