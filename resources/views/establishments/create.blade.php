@extends('neon')


@section('title')
<h2>Establishments</h2>
@endsection

@section('action')
    <a href="javascript:;" onclick="window.history.back();" class="btn btn-primary">
        Go Back 
    </a>
@endsection

@section('content')
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        <br>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br>
    @endif

    <form action="{{ route('establishments.store') }}" method="POST">
        @csrf
        <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                    
            <div class="panel-heading">
                <div class="panel-title">Create Establishment</div>
            </div>
            
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name1">Name of Establishment</label>
                            <input type="text" name="name" class="form-control" id="name1" placeholder="" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nature-of-business">Nature of Business</label>
                            <input type="text" name="description" class="form-control" id="nature-of-business" placeholder="">
                        </div>  
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputAddress">Address</label>
                            <input type="text" name="address" class="form-control" id="inputAddress" placeholder="" required>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="owner-name">Owner Name</label>
                            <input type="text" name="owner_name" class="form-control" id="owner-name" placeholder="" required>
                        </div>  
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="" required>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="">
                        </div>  
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary btn-icon">Register <i class="entypo-check"></i></button>  
            </div>
            
        </div>
    </form>
@endsection
