@extends('neon')

@section('title')
<h2>{{ $establishment->name }}</h2>
@endsection

@section('action')
    <a class="btn btn-primary icon-left btn-icon" href="{{ route('establishments.index') }}">
        Go Back <i class="entypo-left-open"></i>
    </a>
@endsection

@section('content')

<br>
<div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->

    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">Certifications</div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <?php 
        $numOfCols = 4;
        $rowCount = 0;
        $bootstrapColWidth = 12 / $numOfCols;

        ?>
       <div class="row">

            @foreach( $forms as $form )
                <?php $cert = $establishment->cert($form->id); ?>
                <div class="col-sm-3">
                    <div class="tile-stats tile-{{ ($cert) ? $status[$cert->status]:'gray' }}">
                        <div class="icon"><i class="entypo-newspaper"></i></div>
                        
                        @if($cert)
                            <h3>{{ $form->name }}</h3>
                            <p>{{ $message[$cert->status] }}</p><br>
                            @if($cert->status=='expired')
                                <a class="btn btn-xs btn-danger btn-icon" href="{{ route('forms.show', $form->id) }}?es={{ $establishment->id }}">Request Renewal <i class="entypo-right-open"></i></a>
                            @endif
                            @if($cert->status=='pending')
                                <a class="btn btn-xs btn-default btn-icon" href="{{ route('certifications.show',$cert->id) }}">View details <i class="entypo-right-open"></i></a>
                            @endif
                            @if($cert->status=='approved')

                                <a class="btn btn-xs btn-default btn-icon" href="{{ route('certifications.show',$cert->id) }}?print=1" target="_blank">Print Permit<i class="entypo-print"></i></a>
                                @if($cert->valid_until)
                                <span class="btn btn-xs btn-link" style="color:#fff;">
                                Valid Until: <b>{{ \Illuminate\Support\Carbon::parse($cert->valid_until)->format('F j, Y') }}</b>
                                </span>
                                @endif
                            @endif
                        @else
                            <h4>{{ $form->name }}</h4><br>
                            <a href="{{ route('forms.show', $form->id) }}?es={{ $establishment->id }}" class="btn btn-info">Get Certification</a>
                        @endif
                    </div>
                </div>
                <?php 
                $rowCount++;
                if($rowCount % $numOfCols == 0) echo '</div><div class="row">'; ?>
            @endforeach
            
        </div>

    </div>

</div>

@endsection
