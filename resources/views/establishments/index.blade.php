@extends('neon')


@section('title')
<h2>Establishments</h2>
@endsection

@section('action')
    {{-- @can('establishment-create')
        <a href="{{ route('establishments.create') }}" class="btn btn-primary btn-icon">
            Register Establishement <i class="entypo-plus"></i>
        </a>
    @endcan --}}
@endsection

@section('content')
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        <br>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br>
    @endif

    <div class="row">
        
        <div class="col-md-9">
            <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                <div class="panel-heading">
                    <div class="panel-title">Establishments</div>
                </div>
                
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-md-3">

                            <form action="{{ route('establishments.index') }}" method="GET">
                                <div class="input-group">
                                    <input type="text" name="s" value="{{ $keyword }}" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="entypo-search"></i>
                                        </button>
                                    </span>
                                </div>

                            </form>
                        </div>
                        <div class="col-md-9">
                            @can('establishment-create')
                            <a href="{{ route('establishments.create') }}" class="btn btn-primary btn-icon pull-right">
                                Register Establishement <i class="entypo-plus"></i>
                            </a>
                            @endcan
                        </div><!-- comment -->
                    </div>
                    
                    <hr>

                    @if( $establishments->count() )

                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Owner</th>
                                <th>Phone</th>
                                <th width="280px" class="text-right">Action</th>
                            </tr>
                            @foreach ($establishments as $establishment)
                            <tr>
                                <td>
                                    <a class="btn btn-link btn-xs" href="{{ route('establishments.show',$establishment->id) }}">
                                        {{ $establishment->name }}
                                    </a>
                                </td>
                                <td>{{ $establishment->address }}</td>
                                <td>{{ $establishment->owner_name }}</td>
                                <td>{{ $establishment->phone }}</td>
                                <td>
                                    
                                    <a class="btn btn-info btn-xs btn-icon" href="{{ route('establishments.show',$establishment->id) }}">View Certifications <i class="entypo-eye"></i></a>
                                    @can('establishment-edit')
                                    <a class="btn btn-primary btn-xs btn-icon" href="{{ route('establishments.edit',$establishment->id) }}">Edit <i class="entypo-pencil"></i></a>
                                    @endcan
                                    @can('establishment-delete')
                                    <a href="javascript:;" onclick="jQuery('#modal-{{$establishment->id}}').modal('show');" class="btn btn-danger btn-xs btn-icon">Delete <i class="entypo-cancel"></i></a>
                                    @endcan
                                
                                    <div class="modal fade" id="modal-{{$establishment->id}}">
                                        <div class="modal-dialog">
                                            <form action="{{ route('establishments.destroy',$establishment->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-content">
                                                    
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Delete</h4>
                                                    </div>
                                                    
                                                    <div class="modal-body">
                                                        Are you sure you wish to delete the establishment <b>{{ $establishment->name }}</b> owned by by <b>{{ $establishment->owner_name }}</b>? 
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </table>

                        {!! $establishments->links() !!}
                        
                    @else 

                        <div class="alert alert-info alert-dismissible show" role="alert">
                            @if($keyword)
                            <p>No results found.</p>
                            @else
                            <p>
                                You do not have any establishments registered at the moment. <br>
                                You may now start adding here: </p><br><a href="{{ route('establishments.create') }}" class="btn btn-primary btn-icon">
                                Register Your Establishement <i class="entypo-plus"></i>
                            </a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            @endif
                        </div>

                    @endif
                    
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                <div class="panel-heading">
                    <div class="panel-title">Upcoming Events</div>
                </div>
                <div class="panel-body">

                    @if( $events->count() )
                       
                        @foreach ($events as $event)

                            <div class="tile-stats tile-white-gray">
                                <div class="icon"><i class="entypo-calendar"></i></div>
                                <div class="num">{{ $event->name }}</div>
                                <h3>{{ $event->description }}</h3>
                                <p>{{ date('F d, Y', strtotime($event->start_date)) }}</p>
                            </div>
                                
                        @endforeach

                        {!! $events->links() !!}

                    @else 
                        <div class="alert alert-info" role="alert">
                          There are no events to display at the moment.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    

    

    

    

    

@endsection
