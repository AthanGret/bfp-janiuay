@extends('neon')

@section('title')
<h2>User Role Management</h2>
@endsection

@section('action')
    <a class="btn btn-success btn-icon" href="{{ route('roles.create') }}"> Create New Role <i class="entypo-plus"></i></a>
@endsection


@section('content')

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-hover">
  <tr>
     <th>Name</th>
     <th width="280px">Action</th>
  </tr>
    @foreach ($roles as $key => $role)
    <tr>
        <td>{{ $role->name }}</td>
        <td>
            <a class="btn btn-info btn-xs btn-icon" href="{{ route('roles.show',$role->id) }}">Show <i class="entypo-eye"></i></a>
            @can('role-edit')
                <a class="btn btn-primary btn-xs btn-icon" href="{{ route('roles.edit',$role->id) }}">Edit <i class="entypo-pencil"></i></a>
            @endcan
            @can('role-delete')
                <a href="javascript:;" onclick="jQuery('#modal-{{$role->id}}').modal('show');" class="btn btn-danger btn-xs btn-icon">Delete <i class="entypo-cancel"></i></a>
                <div class="modal fade" id="modal-{{$role->id}}">
                    <div class="modal-dialog">
                        <form action="{{ route('roles.destroy',$role->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Delete</h4>
                                </div>
                                
                                <div class="modal-body">
                                    Are you sure you wish to delete this role?
                                </div>
                                
                                <div class="modal-footer">
                                    <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endcan
        </td>
    </tr>
    @endforeach
</table>


{!! $roles->render() !!}

@endsection