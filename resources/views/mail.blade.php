@if($name)
Name: {{$name}}
@endif

@if($email)
Email: {{$email}}
@endif

@if($subject)
Subject: {{$subject}}
@endif

@if($content)
Message:
{{$content}}
@endif
