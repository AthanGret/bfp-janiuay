<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="SG Reporting" />
	<meta name="author" content="" />
	<link rel="icon" href="{{asset('assets/images/favicon.ico')}}">
	<title>{{ config('app.name', 'Bureau Of Fire Protection Management System') }}</title>
	<link rel="stylesheet" href="{{asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-core.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-theme.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-forms.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
	<script src="{{asset('assets/js/jquery-1.11.3.min.js')}}"></script>
	<!--[if lt IE 9]><script src="{{asset('assets/js/ie8-responsive-file-warning.js')}}"></script><![endif]-->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	@yield('header')
</head>
<body class="page-body  page-left-in" data-url="https://reporting.seogstage.com">

<div class="page-container horizontal-menu"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

	<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->

		<div class="navbar-inner">

			<!-- logo -->
			<div class="navbar-brand">
				<a href="/">
					<img src="{{ asset('assets/images/logo.png') }}" alt="">
				</a>
			</div>

			<!-- main menu -->
			<ul class="navbar-nav">
				@can('certification-list')
				<li class="{{ request()->is('certifications*') ? 'active' : '' }}">
					<a href="{{ route('certifications.index') }}">
						<i class="entypo-user"></i>
						<span class="title">Certifications</span>
					</a>
				</li>
				@endcan
				@can('form-delete')
				<li class="{{ request()->is('forms*') ? 'active' : '' }}">
					<a href="{{ route('forms.index') }}">
						<i class="entypo-newspaper"></i>
						<span class="title">Forms</span>
					</a>
				</li>
				@endcan
				@can('establishment-list')
				<li class="{{ request()->is('establishments*') ? 'active' : '' }}">
					<a href="{{ route('establishments.index') }}">
						<i class="entypo-home"></i>
						<span class="title">Establishments</span>
					</a>
				</li>
				@endcan
				@can('event-create')
				<li class="{{ request()->is('events*') ? 'active' : '' }}">
					<a href="{{ route('events.index') }}">
						<i class="entypo-calendar"></i>
						<span class="title">Events</span>
					</a>
				</li>
				@endcan
				@can('user-list')
				<li class="{{ request()->is('users*') ? 'active' : '' }}">
					<a href="{{ route('users.index') }}">
						<i class="entypo-users"></i>
						<span class="title">Users</span>
					</a>
				</li>
				@endcan
				<li class="{{ request()->is('settings') ? 'active' : '' }}">
					<a href="{{ route('settings') }}">
						<i class="entypo-cog"></i>
						<span class="title">Settings</span>
					</a>
				</li>
				{{-- @can('role-list')
				<li class="{{ request()->is('roles*') ? 'active' : '' }}">
					<a href="{{ route('roles.index') }}">
						<i class="entypo-lock-open"></i>
						<span class="title">User Roles</span>
					</a>
				</li>
				@endcan --}}
			</ul>

			<!-- notifications and other links -->
			<ul class="nav navbar-right pull-right">
				<li>
					<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
						{{ __('Log Out') }} <i class="entypo-logout right"></i>
					</a>
				</li>
			</ul>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
		</div>

	</header>

	<div class="main-content">
				
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
				@yield('title')
			</div>
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
				<ul class="list-inline links-list pull-right">
					<li>
						@yield('action')
					</li>
				</ul>
			</div>
		</div>
	
		@yield('content')

		<!-- Footer -->
		<footer class="main">
			
			&copy; {{ date('Y') }} Bureau Of Fire Protection Management System
		
		</footer>
	</div>
</div>

	<script src="{{asset('assets/js/gsap/TweenMax.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/js/joinable.js')}}"></script>
	<script src="{{asset('assets/js/resizeable.js')}}"></script>
	<script src="{{asset('assets/js/neon-api.js')}}"></script>
	<script src="{{asset('assets/js/neon-custom.js')}}"></script>
	<script src="{{asset('assets/js/neon-demo.js')}}"></script>
	@yield('footer')
</body>
</html>