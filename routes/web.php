<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PublicController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CertificationController;
use App\Http\Controllers\EstablishmentController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\FormsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class,'index'])->name('public');
Route::post('/sendmail', [PublicController::class,'sendmail'])->name('sendmail');
Route::get('/cron', [PublicController::class,'cron'])->name('cron');

Route::get('/dashboard', function () {
    return view('neon');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/settings', [HomeController::class, 'settings'])->name('settings');
Route::patch('/settings/update', [HomeController::class, 'settings_update'])->name('settings_update');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('certifications', CertificationController::class);
    Route::resource('establishments', EstablishmentController::class);
    Route::resource('events', EventController::class);
    Route::resource('forms', FormsController::class);
});