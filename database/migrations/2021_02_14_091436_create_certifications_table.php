<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certifications', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->json('contents')->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_until')->nullable();
            $table->enum('status',[
                'pending', // after form is submitted
                'approved', // approved and valid
                'expired', // expired and invalid
            ])->default('pending');
            $table->integer('user_id')->nullable(); // id of the owner
            $table->integer('form_id')->nullable(); // id of the form filled
            $table->integer('establishment_id')->nullable(); // id of the establishment
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certifications');
    }
}
