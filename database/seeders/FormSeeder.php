<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Form;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $forms = array(
            [
                'slug' => 'single-and-two-family-dewlling-checklist', 
                'name' => 'Single And Two Family Dwelling',
                'view' => 'forms.dwelling'
            ],
            // [
            //     'slug' => 'miscelanious-occupancy-checklist', 
            //     'name' => 'Miscelanious Occupancy Checklist',
            //     'view' => 'forms.miscelanious'

            // ],
            // [
            //     'slug' => 'storage-occupancy-checklist', 
            //     'name' => 'Storage Occupancy Checklist',
            //     'view' => 'forms.storage'
            // ],
            // [
            //     'slug' => 'small-general-business-establishment-checklist', 
            //     'name' => 'Small/General Business Establishment Checklist',
            //     'view' => 'forms.small-general'
            // ],
            // [
            //     'slug' => 'business-occupancy-checklist', 
            //     'name' => 'Business Occupancy Checklist',
            //     'view' => 'forms.business'
            // ],
            // [
            //     'slug' => 'places-of-assembly-occupancy-checklist', 
            //     'name' => 'Places of Assembly Occupancy Checklist',
            //     'view' => 'forms.places-of-assembly'
            // ],
            // [
            //     'slug' => 'industrial-occupancy-checklist', 
            //     'name' => 'Industrial Occupancy Checklist',
            //     'view' => 'forms.industrial'
            // ],
            // [
            //     'slug' => 'mercantile-occupancy-checklist', 
            //     'name' => 'Mercantile Occupancy Checklist',
            //     'view' => 'forms.mercantile'
            // ],
            // [
            //     'slug' => 'residential-occupancy-checklist', 
            //     'name' => 'Residential Occupancy Checklist',
            //     'view' => 'forms.residential'
            // ],
            // [
            //     'slug' => 'healthcare-occupancy-checklist', 
            //     'name' => 'Healthcare Occupancy Checklist',
            //     'view' => 'forms.healthcare'
            // ],
            [
                'slug' => 'fire-safety-inspection-certificate', 
                'name' => 'Fire Safety Inspection Certificate',
                'view' => 'forms.fire-safety'
            ]
        );

        foreach( $forms as $form ):
            Form::create($form);
        endforeach;

    }
}
