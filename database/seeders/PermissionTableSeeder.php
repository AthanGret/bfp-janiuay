<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
 
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            'user-list', // 1
            'user-create', // 2
            'user-edit', // 3
            'user-delete', // 4

            'role-list', // 5
            'role-create', // 6
            'role-edit', // 7
            'role-delete', // 8

            'event-list', // 9
            'event-create', // 10
            'event-edit', // 11
            'event-delete', // 12

            'certification-list', // 13
            'certification-create', // 14
            'certification-edit', // 15
            'certification-delete', // 16

            'establishment-list', // 17
            'establishment-create', // 18
            'establishment-edit', // 19
            'establishment-delete', // 20

            'form-list', // 21
            'form-create', // 22
            'form-edit', // 23
            'form-delete', // 24
        ];
     
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
