<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'BFP Admin', 
            'email' => 'admin@bfpj.com',
            'password' => bcrypt('secret')
        ]);
    
        $role = Role::create(['name' => 'Admin']);
     
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);

        $employee = User::create([
            'name' => 'Employee', 
            'email' => 'employee@bfpj.com',
            'password' => bcrypt('secret')
        ]);
    
        $employee_role = Role::create(['name' => 'Employee']);
     
        $employee_perm = [
            // 9,10,11,12, // can manage events
            14,15,16, // can manage certifications
            17,18,19,20, // can manage establishments
            21 // can view form
        ];

        $employee_role->syncPermissions($employee_perm);
        $employee->assignRole([$employee_role->id]);

        $customer = User::create([
            'name' => 'Jonathan', 
            'email' => 'customer@bfpj.com',
            'password' => bcrypt('secret')
        ]);
    
        $customer_role = Role::create(['name' => 'Client']);
     
        $customer_perm = [
            9, // can view events
            14, // can create certification
            17,18,19,20, // can manage establishments
            21 // can view form
        ];
   
        $customer_role->syncPermissions($customer_perm);
        $customer->assignRole([$customer_role->id]);
    }
}
